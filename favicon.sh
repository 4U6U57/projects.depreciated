#!/bin/bash

# Program variables
Exe=$(basename $0 .sh)

# Error function
Error() {
  echo "$Exe: ERROR: $@" >&2
}

# Check dependences
if ! command -v repo-iconify >/dev/null; then
  Error "dependency repo-iconify not installed"
  exit 1
fi
if ! command -v convert >/dev/null; then
  Error "WARNING: dependency imagemagick not installed"
  sudo apt install imagemagick -y
fi
if ! command -v convert >/dev/null; then
  Error "Could not install dependencies"
  exit 1
fi

repo-iconify
convert logo.png -define icon:auto-resize=64,48,32,16 _includes/favicon.ico
