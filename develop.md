---
title: Develop
weight: 2
---

Projects in the **Develop** stage are currently being worked on, but have not reached the point of a fully functional release. They should already have their own repositories in which to hold their codebase.
