---
title: Archive
weight: 6
---

An **Archived** project is one which has been backlogged indefinitely, for one or more of the following reasons:

- Project has no inherent benefit
    - Example: An existing, better solution to the problem already exists.
    - Example: The problem the project is attempting to solve does not or no longer exists.
    - Example: The project idea is inherently or intentionally bad.
- Project is infeasible or impossible.
    - This may be in reference to the scope of the skills of the team, not necessarily  impossible in general.

It is not impossible, but highly unlikely, for an archived project to be upgraded to the backlog or develop stage.
