---
title:  "Projects List"
parent: deployed.md
---

A repository (this repository) listing side project ideas that I have.

### Problem

I often come up with ideas for side projects, but they often come at a time where I have other more pressing work to complete. Listing these side projects in a repository would allow me to keep track of them, as well as encourage me to flesh them out and implement them.

### Details

N/A

### Status

Eternally in progress.
