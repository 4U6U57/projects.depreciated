---
title: Backlog
weight: 4
---

A project in the **Backlog** has been proposed and judged to be low priority. Planning may continue, but at a slower pace than it would if the project was in the Staging area. Projects that have been backlogged for an extended period of time should be moved to the Archive.
