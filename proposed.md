---
title: Proposed
weight: 5
---

A project that is **Proposed** is in it's infancy, consisting of only a title, summary, and possibly a problem statement. Proposed projects should be moved to the Backlog or  Staging area preferably within one week of inception, or as soon as they are fully formulated.
