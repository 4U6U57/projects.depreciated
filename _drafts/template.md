---
title: Proposal Template (title goes here)
parent: STATUS.md
---

Short description of project.

### Problem

A paragraph detailing what the problem is, and how the project aims to solve that problem. A user facing description of the project.

### Details

Details/specification on the project, how the solution will be accomplished. A developer facing description of the project.

### Status

Short description of project development status. Note that the `parent: STATUS.md` field  already sets the general status of the project, and does not need restating.

- [  ] Optional: List of TODO's
