---
layout: page
title: Home
---

A repository of all the project ideas I will work on... eventually.

### Pipeline

Here is the general pipeline for a proposal in this repository. Open the page for a particular status for a description.

![Project Pipeline](http://www.gravizo.com/svg?@startuml;[*]%20--%3E%20Proposed;Proposed%20-%3E%20Staging;Staging-%3EArchive;Proposed%20-%3E%20Backlog;Proposed%20-%3E%20Archive;Backlog%20-%3E%20Staging;Staging%20-%3E%20Develop;Develop%20-%3E%20Deployed;Deployed%20-%3E%20[*];@enduml)

### Repo

This site is deployed on [GitLab Pages](https://4U6U57.gitlab.io/projects) from the repository [4U6U57/projects](https://gitlab.com/4U6U57/projects).
