---
title: Staging
weight: 3
---

Projects that are in the **Staging** area are in the planning stage. At this point, the project specification should still be flexible enough to accept outside input.
