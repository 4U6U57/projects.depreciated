---
title: Deployed
weight: 1
---

Projects marked **Deployed** have already been released, and their documentation is kept only for reference, with full project documentation being available in their respective repository.
